/**
 * @file
 * Automatically submit the payment redirect form.
 */

(function ($) {
    Drupal.behaviors.samanPayment = {
        attach: function (context, settings) {
            $('div.paymentmethodsaman-redirect-form form', context).submit();
        }
    };
})(jQuery);
